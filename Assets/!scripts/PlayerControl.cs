﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{
    public enum FacingSide { Left = -1, Right = 1 };

	public bool enableKeyboard = false;

    [HideInInspector]
    public FacingSide facingSide = FacingSide.Right;    // For determining which way the player is currently facing.
	[HideInInspector]
	public bool jump = false;				            // Condition for whether the player should jump.
	[HideInInspector]
	public float horizontalMove;				        // Player moving direction.


	public float moveForce = 365f;			// Amount of force added to move the player left and right.
	public float maxSpeed = 5f;				// The fastest the player can travel in the x axis.
	
	public float jumpForce = 1000f;			// Amount of force added when the player jumps.
	
	private Transform groundCheck;			// A position marking where to check if the player is grounded.
	private bool grounded = false;			// Whether or not the player is grounded.

    private LayerMask groundLayer;          // Слой объектов, по которым можно ходить


	void Awake()
	{
		groundCheck = transform.Find("groundCheck");
        groundLayer = 1 << LayerMask.NameToLayer("Ground");
	}


	void Update()
	{
		// The player is grounded if a linecast to the groundcheck position hits anything on the ground layer.
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, groundLayer);  

		if(enableKeyboard)
		{
			if(Input.GetKey(KeyCode.A))
				b_moveLeft();
			if(Input.GetKey(KeyCode.D))
				b_moveRight();
			if(Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
				b_moveLeftRightStop();

			if(Input.GetKeyDown(KeyCode.W))
                b_moveJump();
		}

	}


	public void b_moveLeft()
	{
		horizontalMove = -1;
	}

	public void b_moveRight()
	{
		horizontalMove = 1;
	}

    public void b_moveLeftRightStop()
	{
		horizontalMove = 0;
	}

	public void b_moveJump()
	{
		if(grounded)
			jump = true;
	}


	void FixedUpdate ()
	{
        ControlDirectionsMove();

        ControlJumpMove();
	}

    void ControlDirectionsMove()
    {
        // Cache the horizontal input.
        float h = horizontalMove;

        // Если игрок отпустил кнопку передвжения
        if (h == 0)
        {
            rigidbody2D.velocity = new Vector2(0, rigidbody2D.velocity.y);
            return;
        }

        // If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
        if (h * rigidbody2D.velocity.x < maxSpeed)
            // ... add a force to the player.
            rigidbody2D.AddForce(Vector2.right * h * moveForce);

        // If the player's horizontal velocity is greater than the maxSpeed...
        if (Mathf.Abs(rigidbody2D.velocity.x) > maxSpeed)
            // ... set the player's velocity to the maxSpeed in the x axis.
            rigidbody2D.velocity = new Vector2(Mathf.Sign(rigidbody2D.velocity.x) * maxSpeed, rigidbody2D.velocity.y);

        // If the input is moving the player to the other side then the player is facing...
        if (h != (float)facingSide)
            // ... flip the player.
            Flip();
    }

    void ControlJumpMove()
    {
        // If the player should jump...
        if (jump)
        {
            // Add a vertical force to the player.
            rigidbody2D.AddForce(new Vector2(0f, jumpForce));

            // Make sure the player can't jump again until the jump conditions from Update are satisfied.
            jump = false;
        }
    }

	void Flip ()
	{
		// Switch the way the player is labelled as facing.
        switch (facingSide)
        {
            case FacingSide.Right:
                facingSide = FacingSide.Left;
                break;
            case FacingSide.Left:
                facingSide = FacingSide.Right;
                break;
        }

		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
}
